﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using System.Threading.Tasks;
using FSAStore.Views;
using FSAStore.iOS.Renderers;

[assembly: ExportRenderer(typeof(RootTabbedPage), typeof(ExtendedTabbedPage))]
namespace FSAStore.iOS.Renderers
{
	public class ExtendedTabbedPage : TabbedRenderer
	{
		UITabBarController tabbedController;
		UISwipeGestureRecognizer rightGesture, leftGesture;
		bool isInitialized = false;

		protected override void OnElementChanged(VisualElementChangedEventArgs e)
		{
			base.OnElementChanged(e);

			if (e.NewElement != null)
			{
				tabbedController = (UITabBarController)ViewController;
			}
			UITabBar.Appearance.TintColor = UIColor.White;
			UITabBar.Appearance.BarTintColor = UIColor.FromRGB(31, 121, 177);

		}
	}
}