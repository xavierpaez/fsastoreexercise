﻿using System;
using Facebook.LoginKit;
using FSAStore.Services;
using Facebook;
using UIKit;
using System.Threading.Tasks;
using System.Linq;
using Facebook.ShareKit;
using Foundation;
using Facebook.CoreKit;
using System.Collections.Generic;
using ExternalAccessory;
using System.IO;
using AVFoundation;

namespace FSAStore.iOS.Services
{
	public class FacebookService : NSObject, IFacebookService, ISharingDelegate
	{
		public event EventHandler<FBEventArgs<Dictionary<string, object>>> OnUserData = delegate { };

		public event EventHandler<FBEventArgs<bool>> OnLogin = delegate { };

		public event EventHandler<FBEventArgs<bool>> OnLogout = delegate { };

		public event EventHandler<FBEventArgs<Dictionary<string, object>>> OnSharing = delegate { };

		LoginManager loginManager;
		FacebookPendingAction<Dictionary<string, object>> pendingAction;


		public string[] ActivePermissions
		{
			get
			{
				return AccessToken.CurrentAccessToken == null ? new string[] { } : AccessToken.CurrentAccessToken.Permissions.Select(p => $"{p}").ToArray();
			}
		}

		public string[] DeclinedPermissions
		{
			get
			{
				return AccessToken.CurrentAccessToken == null ? new string[] { } : AccessToken.CurrentAccessToken.DeclinedPermissions.Select(p => $"{p}").ToArray();
			}
		}

		public bool IsLoggedIn
		{
			get
			{
				return AccessToken.CurrentAccessToken != null;
			}
		}

		public string ActiveToken
		{
			get
			{
				return AccessToken.CurrentAccessToken?.TokenString ?? string.Empty;
			}
		}

		public string ActiveUserId
		{
			get
			{
				return AccessToken.CurrentAccessToken?.UserID ?? string.Empty;
			}
		}

		public FacebookService()
		{
			loginManager = new LoginManager();
		}


		public bool HasPermissions(string[] permissions)
		{
			if (!IsLoggedIn)
				return false;

			var currentPermissions = AccessToken.CurrentAccessToken.Permissions;

			return permissions.All(p => AccessToken.CurrentAccessToken.HasGranted(p));
		}

		public bool VerifyPermission(string permission)
		{

			return IsLoggedIn && (AccessToken.CurrentAccessToken.HasGranted(permission));

		}

		public void DidComplete(ISharing sharer, NSDictionary results)
		{
			Dictionary<string, object> parameters = new Dictionary<string, object>();
			foreach (var r in results)
			{
				parameters.Add($"{r.Key}", $"{r.Value}");
			}
			OnSharing(this, new FBEventArgs<Dictionary<string, object>>(parameters, FacebookActionStatus.Completed));
		}

		public void DidFail(ISharing sharer, NSError error)
		{
			OnSharing(this, new FBEventArgs<Dictionary<string, object>>(null, FacebookActionStatus.Error, error.Description));
		}

		public void DidCancel(ISharing sharer)
		{

			OnSharing(this, new FBEventArgs<Dictionary<string, object>>(null, FacebookActionStatus.Canceled));
		}

		public async Task<bool> LoginAsync(string[] permissions, FacebookPermissionType permissionType = FacebookPermissionType.Read)
		{
			var retVal = IsLoggedIn;
			FacebookActionStatus status = FacebookActionStatus.Error;
			if (!retVal || !HasPermissions(permissions))
			{
				var window = UIApplication.SharedApplication.KeyWindow;
				var vc = window.RootViewController;
				while (vc.PresentedViewController != null)
				{
					vc = vc.PresentedViewController;
				}

				LoginManagerLoginResult result = await (permissionType == FacebookPermissionType.Read ? loginManager.LogInWithReadPermissionsAsync(permissions, vc) : loginManager.LogInWithPublishPermissionsAsync(permissions, vc));

				if (result.IsCancelled)
				{
					retVal = false;
					status = FacebookActionStatus.Canceled;
				}
				else
				{
					//result.GrantedPermissions.h
					retVal = HasPermissions(result.GrantedPermissions.Select(p => $"{p}").ToArray());

					status = retVal ? FacebookActionStatus.Completed : FacebookActionStatus.Unauthorized;
				}

			}
			else
			{

				status = FacebookActionStatus.Completed;

			}

			OnLogin(this, new FBEventArgs<bool>(retVal, status));

			pendingAction?.Execute();

			pendingAction = null;


			return retVal;
		}

		void RequestUserData(Dictionary<string, object> fieldsDictionary)
		{
			string[] fields = new string[] { };
			var extraParams = string.Empty;
			if (fieldsDictionary != null && fieldsDictionary.ContainsKey("fields"))
			{
				fields = fieldsDictionary["fields"] as string[];
				if (fields.Length > 0)
					extraParams = $"?fields={string.Join(",", fields)}";
			}

			//email,first_name,gender,last_name,birthday

			if (AccessToken.CurrentAccessToken != null)
			{
				Dictionary<string, object> userData = new Dictionary<string, object>();
				var graphRequest = new GraphRequest($"/me{extraParams}", null, AccessToken.CurrentAccessToken.TokenString, null, "GET");
				var requestConnection = new GraphRequestConnection();
				requestConnection.AddRequest(graphRequest, (connection, result, error) =>
				{
					if (error == null)
					{
						for (int i = 0; i < fields.Length; i++)
						{
							userData.Add(fields[i], $"{result.ValueForKey(new NSString(fields[i]))}");
						}
						userData.Add("user_id", AccessToken.CurrentAccessToken.UserID);
						userData.Add("token", AccessToken.CurrentAccessToken.TokenString);
						OnUserData(this, new FBEventArgs<Dictionary<string, object>>(userData, FacebookActionStatus.Completed));
					}
					else
					{
						OnUserData(this, new FBEventArgs<Dictionary<string, object>>(null, FacebookActionStatus.Error, $"Facebook User Data Request Failed - {error.Code} - {error.Description}"));

					}


				});
				requestConnection.Start();
			}
			else {
				OnUserData(this, new FBEventArgs<Dictionary<string, object>>(null, FacebookActionStatus.Canceled));
			}


		}


		async void PerformAction(Action<Dictionary<string, object>> action, Dictionary<string, object> parameters, FacebookPermissionType permissionType, string[] permissions)
		{
			pendingAction = null;

			bool authorized = HasPermissions(permissions);

			if (!authorized)
			{
				pendingAction = new FacebookPendingAction<Dictionary<string, object>>(action, parameters);
				await LoginAsync(permissions, permissionType);
			}
			else {
				action(parameters);
			}


		}

		public void Logout()
		{
			if (IsLoggedIn)
				loginManager.LogOut();

			OnLogout(this, new FBEventArgs<bool>(true, FacebookActionStatus.Completed));
		}

		public void RequestUserData(string[] fields, string[] permissions, FacebookPermissionType permissionType = FacebookPermissionType.Read)
		{
			Dictionary<string, object> parameters = new Dictionary<string, object>()
			{
				{"fields",fields}
			};

			PerformAction(RequestUserData, parameters, FacebookPermissionType.Read, permissions);
		}

	}
}
