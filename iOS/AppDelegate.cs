﻿using System;
using System.Collections.Generic;
using System.Linq;
using Acr.UserDialogs;
using Facebook.CoreKit;
using FFImageLoading.Forms.Touch;
using Foundation;
using FSAStore.iOS.Services;
using FSAStore.Services;
using Microsoft.Practices.Unity;
using Prism.Unity;
using UIKit;

namespace FSAStore.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		static FacebookService fbService = new FacebookService();
		private const string FacebookAppId = "181299692378536";
		private const string FacebookAppName = "FSAStoreDemo";

		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			
			global::Xamarin.Forms.Forms.Init();

			CachedImageRenderer.Init();
			CustomizeAppearance();

			// Code for starting up the Xamarin Test Cloud Agent
#if ENABLE_TEST_CLOUD
			Xamarin.Calabash.Start();
#endif
			Settings.AppID = FacebookAppId;
			Settings.DisplayName = FacebookAppName;
			LoadApplication(new App(new iOSInitializer()));

			return base.FinishedLaunching(app, options);
		}

		public override void OnResignActivation(UIApplication uiApplication)
		{
			base.OnResignActivation(uiApplication);
			AppEvents.ActivateApp();
		}

		public class iOSInitializer : IPlatformInitializer
		{
			public void RegisterTypes(IUnityContainer container)
			{

				container.RegisterInstance<IUserDialogs>(UserDialogs.Instance, new ExternallyControlledLifetimeManager());
				container.RegisterInstance<IFacebookService>(fbService, new ExternallyControlledLifetimeManager());

			}
		}

		public void CustomizeAppearance()
		{
			UINavigationBar.Appearance.BarTintColor = UIColor.FromRGB(218, 34, 08);
			UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes
			{
				TextColor = UIColor.White
			});

			UINavigationBar.Appearance.TintColor = UIColor.White;
		}
		public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
		{
			if (url.Scheme.Contains("fsaStoreDemo"))
			{
				return false;

			}
			else {
				return ApplicationDelegate.SharedInstance.OpenUrl(application, url, sourceApplication, annotation);
			}
		}

		#region Background file transfer
		public Action UrlSessionCompletion { get; set; }
		public override void HandleEventsForBackgroundUrl(UIApplication application, string sessionIdentifier, Action completionHandler)
		{
			UrlSessionCompletion = completionHandler;
		}
		#endregion
	}
}
