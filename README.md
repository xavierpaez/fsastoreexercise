
## Installation

Open the code using Xamarin Studio and run the project, for iOS make sure to compile using Deployment Targer (8.0). For Android 
Minimum Android Version (23) and Target Android version (23)
The physical device that I had while testing the app was an iPhone, so I please would suggest to run it with it. 

## Login

Username: fsastore 
password: Demo123!

## API Reference

Walmart Open Api was used for this project, for more information 
https://developer.walmartlabs.com/io-docs