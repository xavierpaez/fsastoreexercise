﻿using System;
using Prism.Mvvm;
using Prism.Navigation;
using FSAStore.Services;
using System.Threading.Tasks;
using Acr.UserDialogs;
using System.Diagnostics;

namespace FSAStore.ViewModels
{
	public class BasePageViewModel : BindableBase
	{

		private bool _isBusy;
		public bool IsBusy
		{
			get { return _isBusy; }
			set { SetProperty(ref _isBusy, value); }
		}

		public IApiManager apiManager;
		public IDBManager dBManager;

		public BasePageViewModel(IApiManager apiManager = null, IDBManager bdManager = null)
		{
			this.apiManager = apiManager;
			this.dBManager = bdManager;
		}

		public async Task RunSafe(Task task, bool ShowLoading = true, string loadinMessage = null)
		{
			try
			{
				IsBusy = true;

				if (ShowLoading) UserDialogs.Instance.ShowLoading(loadinMessage ?? AppResources.LoadingMessage);

				await task;
			}
			catch (TaskCanceledException)
			{
				UserDialogs.Instance.HideLoading();
				Debug.WriteLine("Task Cancelled");
			}
			catch (AggregateException e)
			{
				var ex = e.InnerException;
				while (ex.InnerException != null)
					ex = ex.InnerException;
				UserDialogs.Instance.HideLoading();
				await App.Current.MainPage.DisplayAlert(AppResources.ErrorConnectionTitle, AppResources.ErrorConnectionMessage, "Ok");
			}
			catch (Exception e)
			{
				UserDialogs.Instance.HideLoading();
				Debug.WriteLine(e.ToString());
				await App.Current.MainPage.DisplayAlert(AppResources.ErrorConnectionTitle, AppResources.ErrorConnectionMessage, "Ok");
			}
			finally
			{
				IsBusy = false;
				if (ShowLoading) UserDialogs.Instance.HideLoading();
			}
		}
	}
}
