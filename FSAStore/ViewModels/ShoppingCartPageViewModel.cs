﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Acr.UserDialogs;
using FSAStore.Helpers;
using FSAStore.Models;
using FSAStore.Models.Request;
using FSAStore.Services;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using PropertyChanged;
using System.Linq;
using FSAStore.Models.Requests;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace FSAStore.ViewModels
{
	[ImplementPropertyChanged]
	public class ShoppingCartPageViewModel : BasePageViewModel
	{
		IApiManager apiManager;
		IDBManager dbManager;
		INavigationService navigationService;
		public DelegateCommand PresentHomeCommand { get; set; }
		public DelegateCommand ClearCartCommand { get; set; }

		public ObservableCollection<Product> Products { get; set; } = new ObservableCollection<Product>();
		public string Checkout { get; set; }
		public ShoppingCartPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IApiManager apiManager, IDBManager dbManager) : base(apiManager)
		{
			this.apiManager = apiManager;
			this.dbManager = dbManager;
			this.navigationService = navigationService;
			PresentHomeCommand = new DelegateCommand(async () => await HomePageAsync());
			ClearCartCommand = new DelegateCommand(async () => await DeleteShoppingCart());
			GetProductsCommand.Execute(null);
		}

		public async Task HomePageAsync()
		{
			await navigationService.GoBackAsync(null, true, false);
		}

		public async Task DeleteShoppingCart()
		{
			dbManager.ClearRecentActivity();
			Products.Clear();
			Settings.Products = 0;
			Settings.Total = 0;
			Checkout = $"Shopping Cart Empty";
		}

		public Command GetProductsCommand
		{
			get{
				return new Command(GetProducts);
			}
		}
		private void GetProducts()
		{
			var productList = dbManager.RetrevieCartItems();
			Products = new ObservableCollection<Product>(productList);
			Checkout = $"Continue To Checkout: ${Settings.Total}";
		}

	}
}

