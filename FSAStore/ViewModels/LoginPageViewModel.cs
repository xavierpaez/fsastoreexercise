﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Acr.UserDialogs;
using FSAStore.Helpers;
using FSAStore.Models;
using FSAStore.Models.Request;
using FSAStore.Services;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using PropertyChanged;
using System.Linq;
using FSAStore.Models.Requests;
using Xamarin.Forms;

namespace FSAStore.ViewModels
{
	[ImplementPropertyChanged]
	public class LoginPageViewModel : BasePageViewModel
	{
		IPageDialogService dialogService;
		INavigationService navigationService;
		IFacebookService facebookService;

		public LoginRequest UserData { get; set; } = new LoginRequest();

		public DelegateCommand LoginCommand { get; set; }
		public DelegateCommand LoginFacebookCommand { get; set; }
		public DelegateCommand SignUpCommand { get; set; }
		public DelegateCommand OnForgotPassowrdCommand { get; set; }

		public LoginPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IFacebookService facebookService, IApiManager apiManager) : base(apiManager)
		{
			dialogService = pageDialogService;
			this.navigationService = navigationService;
			this.facebookService = facebookService;

			LoginCommand = new DelegateCommand(async () => await LoginAsync());
			LoginFacebookCommand = new DelegateCommand(async () => await RunSafe(LoginFacebookAsync(), false));

		}

		public async Task LoginAsync()
		{
			var username = UserData?.Username?.Trim();
			if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(UserData.Password))
			{
				await dialogService.DisplayAlertAsync(Config.AppName, AppResources.EmptyFields, "Ok");
			}
			else
			{
				UserDialogs.Instance.ShowLoading(AppResources.LoadingMessage);
				HttpResponseMessage response = await apiManager.Login(UserData.Username, UserData.Password);
				UserDialogs.Instance.HideLoading();

				HttpResponseMessage registerResponse = new HttpResponseMessage() { StatusCode = System.Net.HttpStatusCode.BadRequest };
				var resp = await response.Content.ReadAsStringAsync();
				if (response.StatusCode == System.Net.HttpStatusCode.OK)
				{
					await navigationService.NavigateAsync(NavigationConstants.Home);
				}
				else
				{
					await dialogService.DisplayAlertAsync(Config.AppName, AppResources.WrongUser, "Ok");
				}
			}
		}

		public async Task LoginFacebookAsync()
		{
			if (Device.OS == TargetPlatform.iOS)
			{
				TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
				EventHandler<FBEventArgs<Dictionary<string, object>>> userDataDelegate = null;
				userDataDelegate = async (object sender, FBEventArgs<Dictionary<string, object>> e) =>
				{
					switch (e.Status)
					{
						case FacebookActionStatus.Completed:
							var data = new RegisterRequest();
							data.ExternalLogin = new ExternalLogin();
							data.FirstName = $"{e.Data["first_name"]}";
							data.LastName = $"{e.Data["last_name"]}";
							data.Email = $"{e.Data["email"]}";

							UserDialogs.Instance.ShowLoading(AppResources.LoadingMessage);
							HttpResponseMessage response = await apiManager.LoginWithFacebook(data.Token);
							UserDialogs.Instance.HideLoading();
							await navigationService.NavigateAsync(NavigationConstants.Home);
							break;
					}
					tcs.TrySetResult(true);
					facebookService.OnUserData -= userDataDelegate;
				};

				facebookService.OnUserData += userDataDelegate;
				facebookService.RequestUserData(new string[] { "email", "first_name", "last_name" }, new string[] { "email" });

				await tcs.Task;
			}
		}
	}
}