﻿using System;
using FSAStore.Helpers;
using FSAStore.Services;
using FSAStore.ViewModels;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace FSAStore.ViewModels
{
	public class SettingsPageViewModel : BasePageViewModel
	{
		public DelegateCommand LogoutCommand { get; set; }

		IPageDialogService pageDialogService;
		INavigationService navigationService;
		IDBManager dbManager;

		public SettingsPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IApiManager apiManager, IDBManager dbManager) : base(apiManager)
		{
			this.dbManager = dbManager;
			this.navigationService = navigationService;
			LogoutCommand = new DelegateCommand(GetShoppingCart);
		}

		public async void GetShoppingCart()
		{
			await navigationService.NavigateAsync(NavigationConstants.Login,null, true);
		}

	}
}
