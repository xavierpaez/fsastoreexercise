﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Acr.UserDialogs;
using FSAStore.Helpers;
using FSAStore.Models;
using FSAStore.Services;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using PropertyChanged;
using Xamarin.Forms;

namespace FSAStore.ViewModels
{
	[ImplementPropertyChanged]
	public class ProductsPageViewModel : BasePageViewModel
	{
		IApiManager apiManager;
		IDBManager dbManager;
		public List<Item> Items { get; set;}
		public int ItemsInCart { get; set; } = Settings.Products;

		INavigationService navigationService;

		public DelegateCommand GetShoppingCartCommand { get; set; }

		public ProductsPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IApiManager apiManager, IDBManager dbManager) : base(apiManager)
		{
			this.apiManager = apiManager;
			this.dbManager = dbManager;
			this.navigationService = navigationService;
			GetShoppingCartCommand = new DelegateCommand(GetShoppingCart);
			GetInformation.Execute(null);
		}

		public Item _selectedItem; 
		public Item SelectedItem
		{
			get
			{
				return _selectedItem;
			}
			set
			{
				_selectedItem = value;
				if (value != null)
					ItemSelectedCommand.Execute(value);
			}
		}

		public Command ItemSelectedCommand
		{
			get
			{
				return new Command((item) =>
				{
					 OnSelection(item as Item);
				});
			}
		}

		public async void GetShoppingCart()
		{
			await navigationService.NavigateAsync(NavigationConstants.ShoppingCart);
		}

		public void OnSelection(Item item)
		{
			dbManager.SaveItem(item);
			ItemsInCart = Settings.Products;
		}

		public Command GetInformation
		{
			get
			{
				return new Command(async (param) =>
				{
					if (IsBusy) return;
					await GetData();
				});
			}
		}

		private async Task GetData()
		{
			UserDialogs.Instance.ShowLoading(AppResources.LoadingMessage);
			var response = await base.apiManager.GetProductCategory("Personal Care");
			UserDialogs.Instance.HideLoading();
			if (response.IsSuccessStatusCode)
			{
				var resp = await response.Content.ReadAsStringAsync();
				var deserialize = JsonConvert.DeserializeObject<ProductResponse>(resp);
				Items = new List<Item>(deserialize.Items);
			}
		}


	}
}
