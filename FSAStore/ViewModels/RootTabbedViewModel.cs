﻿using System;
using FSAStore.Services;
using FSAStore.ViewModels;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace FSAStore
{
	public class RootTabbedPageViewModel : BasePageViewModel
	{
		IPageDialogService pageDialogService;
		INavigationService navigationService;
		IDBManager dbManager;

		public DelegateCommand LoginCommand { get; set; }

		public RootTabbedPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IDBManager dbManager)
		{
			this.pageDialogService = pageDialogService;
			this.navigationService = navigationService; ;
			this.dbManager = dbManager;
		}
	}
}
