﻿using System;
using System.Collections.Generic;
using Prism.Events;
using Xamarin.Forms;
using Prism.Navigation;

namespace FSAStore.Views
{
	public partial class RootTabbedPage : TabbedPage
	{
		IEventAggregator _eventAggregator;

		public RootTabbedPage(IEventAggregator eventAggregator)
		{
			InitializeComponent();
			_eventAggregator = eventAggregator;
			_eventAggregator.GetEvent<TabEvent<int>>().Subscribe(OnTabSelected);
			//this.SelectedItem = this.Children[2];
		}

		void OnTabSelected(int index)
		{
			//this.SelectedItem = this.Children[index];
			this.CurrentPage = this.Children[index];
		}
	}
}

