﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace FSAStore.Views
{
	public partial class ShoppingCartPage : ContentPage
	{
		public ShoppingCartPage()
		{
			InitializeComponent();
		}

		public void OnItemSelected(object sender, ItemTappedEventArgs args)
		{
			shoppingCart.SelectedItem = null;
		}

	}
}
