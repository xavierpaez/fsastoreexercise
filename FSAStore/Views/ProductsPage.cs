﻿using System;
using System.Collections.Generic;
using FSAStore.Helpers;
using FSAStore.ViewModels;
using Xamarin.Forms;

namespace FSAStore.Views
{
	public partial class ProductsPage : ContentPage
	{
		public ProductsPage()
		{
			InitializeComponent();
		}

		public void OnItemSelected(object sender, ItemTappedEventArgs args)
		{
			personalCare.SelectedItem = null;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			if (this.BindingContext != null)
			{
				var model = (ProductsPageViewModel)BindingContext;
				model.ItemsInCart = Settings.Products;
			}
		}
	}
}
