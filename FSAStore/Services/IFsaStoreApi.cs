﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using FSAStore.Models.Request;
using Refit;

namespace FSAStore.Services
{
	[Headers("Content-Type: application/json")]
	public interface IFSAStoreApi
	{
		[Post("/api/account/logoutall")]
		Task<HttpResponseMessage> Logout([Header("Authorization")] string authorization);

		[Post("/api/account/FacebookLogin")]
		Task<HttpResponseMessage> FacebookLogin([Body] FBLoginRequest request);

		[Get("/v1/search?query={searchParameter}&format=json&apiKey={apiKey}")]
		Task<HttpResponseMessage> GetProductCategory(string searchParameter, string apiKey);
	}
}
