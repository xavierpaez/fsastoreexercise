﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Refit;

namespace FSAStore.Services
{
	[Headers("Content-Type: application/json", "X-Parse-Application-Id: rnNJOvU4PREbjkJY1DJe0XDkYR829G9kIfV7bKaj", "X-Parse-REST-API-Key: 3EIT81dlr0srBW6CduudmTW7fX4ngKZriKVKOZyb")]
	public interface IBack4App
	{
		[Get("/login?username={username}&password={password}")]
		Task<HttpResponseMessage> Login(string username, string password);
	}
}
