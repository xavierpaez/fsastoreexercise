﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FSAStore.Models;

namespace FSAStore.Services
{
	public interface IDBManager
	{
		void SaveItem(Item item);
		List<Product> RetrevieCartItems();
		void ClearRecentActivity();
	}
}
