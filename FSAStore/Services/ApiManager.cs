﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FSAStore.Models.Request;
using FSAStore.Services;
using Fusillade;
using Plugin.Connectivity.Abstractions;
using Polly;
using Refit;

namespace FSAStore.Services
{
	public class ApiManager : IApiManager
	{
		IApiService<IFSAStoreApi> fsaStoreApi;
		IApiService<IBack4App> back4app;


		public bool IsReachable { get; set; }

		public ApiManager(IApiService<IFSAStoreApi> fsaStoreApi, IApiService<IBack4App> back4app, IConnectivity connectivity)
		{
			this.fsaStoreApi = fsaStoreApi;
			this.back4app = back4app;
			IsReachable = connectivity.IsConnected;
			connectivity.ConnectivityChanged += (sender, e) => IsReachable = e.IsConnected;
		}

		public async Task<HttpResponseMessage> LoginWithFacebook(string token)
		{
			return await RemoteRequestAsync<HttpResponseMessage>(fsaStoreApi.GetApi(Priority.UserInitiated).FacebookLogin(new FBLoginRequest() { Token = token }));
		}

		public async Task<HttpResponseMessage> GetProductCategory(string productCategory)
		{
			return await RemoteRequestAsync<HttpResponseMessage>(fsaStoreApi.GetApi(Priority.UserInitiated).GetProductCategory(productCategory, Config.WallmartOpenApiKey));
		}


		public async Task<HttpResponseMessage> Login(string username, string password)
		{
			return await RemoteRequestAsync<HttpResponseMessage>(back4app.GetApi(Priority.UserInitiated).Login(username, password));
		}

		protected async Task<TData> RemoteRequestAsync<TData>(Task<TData> task)
		{
			TData data = default(TData);

			data = await Policy
				.Handle<WebException>()
				.Or<ApiException>()
				.WaitAndRetryAsync
				(
					retryCount: 1,
					sleepDurationProvider: retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt))
				)
				.ExecuteAsync(async () =>
				   {
					   //ct.ThrowIfCancellationRequested ();
					   var result = await task;
					   //ct.ThrowIfCancellationRequested ();
					   return result;
				   });

			return data;
		}

	}
}
