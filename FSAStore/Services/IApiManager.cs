﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace FSAStore.Services
{
	public interface IApiManager
	{
		Task<HttpResponseMessage> LoginWithFacebook(string token);
		Task<HttpResponseMessage> GetProductCategory(string searchParameter);
		Task<HttpResponseMessage> Login(string username, string password);
	}
}
