﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FSAStore.Helpers;
using FSAStore.Models;
using FSAStore.Services;
using Realms;

namespace FSAStore
{
	public class DBManager : IDBManager
	{
		private Realm realm;

		public DBManager()
		{
			realm = Realm.GetInstance();
		}

		public void SaveItem(Item item)
		{
			var shoppingCartItem = realm.All<Product>().FirstOrDefault(d => d.ItemId == item.ItemId);
			using (var trans = realm.BeginWrite())
			{
				if (shoppingCartItem == null)
				{
					{
						realm.Add(new Product
						{
							Name = item.Name,
							ItemId = item.ItemId,
							Quantity = 1,
							SalePrice = item.SalePrice,
							ShortDescription = item.ShortDescription,
							LongDescription = item.LongDescription,
							MediumImage = item.MediumImage,
							ThumbnailImage = item.ThumbnailImage
						});
						trans.Commit();
					}
				}
				else
				{
					shoppingCartItem.Quantity += 1;
					trans.Commit();

				}
				Settings.Products += 1;
				Settings.Total += item.SalePrice;
			}
		}

		public List<Product> RetrevieCartItems()
		{
			List<Product> products = new List<Product>();
			products = realm.All<Product>().ToList();
			return products;
		}

		public void ClearRecentActivity()
		{
			var transaction = realm.BeginWrite();
			realm.RemoveAll<Product>();
			transaction.Commit();
			Settings.Products = 0;
		}

	}
}
