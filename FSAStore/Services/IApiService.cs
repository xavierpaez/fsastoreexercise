﻿using System;
using Fusillade;

namespace FSAStore.Services
{
	public interface IApiService<T>
	{
		T GetApi(Priority priority);
	}
}
