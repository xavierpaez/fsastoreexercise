﻿using System;
using Prism.Events;
using Xamarin.Forms;

namespace FSAStore
{
	public class TabEvent<T> : PubSubEvent<T>
	{
		public T Index { get; set; }
	}
}

