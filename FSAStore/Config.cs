﻿using System;
namespace FSAStore
{
	public static class Config
	{

		public static ConfigurationType ConfigurationType { get; } = ConfigurationType.Dev;

		public static string ApiUrl
		{
			get
			{
				switch (Config.ConfigurationType)
				{
					case ConfigurationType.Dev:
						return "https://api.walmartlabs.com/";
					default:
						break;
				}

				return "";
			}
		}

		public static string ApiBack4AppUrl
		{
			get
			{
				switch (Config.ConfigurationType)
				{
					case ConfigurationType.Dev:
						return "https://parseapi.back4app.com/";
					default:
						break;
				}

				return "";
			}
		}

		public const string AppName = "FSAStoreDemo";
		public const string WallmartOpenApiKey = "ateb5hwvczve7jutgywuc2jn";
	}

	public enum ConfigurationType
	{
		Production,
		Dev,
		Testing
	}
}
