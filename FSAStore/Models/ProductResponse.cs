﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace FSAStore.Models
{
	public class GiftOptions
	{

		[JsonProperty("allowGiftWrap")]
		public bool AllowGiftWrap { get; set; }

		[JsonProperty("allowGiftMessage")]
		public bool AllowGiftMessage { get; set; }

		[JsonProperty("allowGiftReceipt")]
		public bool AllowGiftReceipt { get; set; }
	}


	public class ProductResponse
	{
		[JsonProperty("query")]
		public string Query { get; set; }

		[JsonProperty("sort")]
		public string Sort { get; set; }

		[JsonProperty("responseGroup")]
		public string ResponseGroup { get; set; }

		[JsonProperty("totalResults")]
		public int TotalResults { get; set; }

		[JsonProperty("start")]
		public int Start { get; set; }

		[JsonProperty("numItems")]
		public int NumItems { get; set; }

		[JsonProperty("items")]
		public IList<Item> Items { get; set; }

		[JsonProperty("facets")]
		public IList<object> Facets { get; set; }
	}
}


