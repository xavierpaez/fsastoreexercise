﻿using System;
using Newtonsoft.Json;

namespace FSAStore.Models
{
	public class ImageEntity
	{
		[JsonProperty("thumbnailImage")]
		public string ThumbnailImage { get; set; }

		[JsonProperty("mediumImage")]
		public string MediumImage { get; set; }

		[JsonProperty("largeImage")]
		public string LargeImage { get; set; }

		[JsonProperty("entityType")]
		public string EntityType { get; set; }
	}
}
