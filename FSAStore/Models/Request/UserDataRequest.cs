﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace FSAStore.Models
{
	public class UserDataResponse
	{
		[JsonProperty("access_token")]
		public string AccessToken { get; set; }

		[JsonProperty("identityId")]
		public string IdentityId { get; set; }

		[JsonProperty("token_type")]
		public string TokenType { get; set; }

		[JsonProperty("expires_in")]
		public int ExpiresIn { get; set; }

		[JsonProperty("userName")]
		public string UserName { get; set; }

		[JsonProperty("names")]
		public string Names { get; set; }

		[JsonProperty("email")]
		public string Email { get; set; }

		[JsonProperty("hasPassword")]
		public bool HasPassword { get; set; }

		[JsonProperty("firstName")]
		public string FirstName { get; set; }

		[JsonProperty("lastName")]
		public string LastName { get; set; }

		[JsonProperty("birthday")]
		public string Birthday { get; set; }

		[JsonProperty("gender")]
		public string Gender { get; set; }

		[JsonProperty("HasPhoto")]
		public bool HasPhoto { get; set; }

		[JsonProperty("message")]
		public string Message { get; set; }

		[JsonProperty("imageUrl")]
		public string ImageUrl { get; set; }

		[JsonProperty("countryId")]
		public string CountryId { get; set; }

		[JsonProperty("cityId")]
		public string CityId { get; set; }

		[JsonProperty("externalAccounts")]
		public List<ExternalAccount> ExternalAccounts { get; set; }

		public class ExternalAccount
		{
			public string LoginProvider { get; set; }
			public string ProviderKey { get; set; }
			public string UserId { get; set; }
		}

	}
}
