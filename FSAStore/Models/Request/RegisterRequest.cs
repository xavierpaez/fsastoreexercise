﻿using System;
using Newtonsoft.Json;
using PropertyChanged;
using FSAStore.Helpers;

namespace FSAStore.Models.Requests
{
	[ImplementPropertyChanged]
	public class RegisterRequest
	{
		[JsonProperty("Email")]
		public string Email { get; set; }

		[JsonProperty("Username")]
		public string Username { get { return Email; } }

		[JsonProperty("FirstName")]
		public string FirstName { get; set; }

		[JsonProperty("LastName")]
		public string LastName { get; set; }

		[JsonProperty("Birthdate")]
		public DateTime BirthDate { get; set; } = DateTime.Now.AddYears(-30);

		[JsonProperty("Password")]
		public string Password { get; set; }

		[JsonProperty("ConfirmPassword")]
		public string ConfirmPassword { get; set; }

		[JsonProperty("Gender")]
		public string Gender { get; set; }

		[JsonProperty("ExternalLogin")]
		public ExternalLogin ExternalLogin { get; set; }

		[JsonProperty("CountryId")]
		public int CountryId { get; set; }

		[JsonProperty("CityId")]
		public int CityId { get; set; }

		[JsonProperty("Code")]
		public string Code { get; set; }

		[JsonIgnore]
		public string Token { get; set; }

		[JsonIgnore]
		public string DayBirthday { get; set; } = "01";

		[JsonIgnore]
		public string MonthBirthday { get; set; } = "01";

		[JsonIgnore]
		public string YearBirthday { get; set; } = "1930";

		[JsonIgnore]
		public bool IsTermsAndConditionsAccepted { get; set; } = true;



	}

	public class ExternalLogin
	{
		[JsonProperty("LoginProvider")]
		public string LoginProvider { get; set; } = SocialProviders.Facebook.ToString();

		[JsonProperty("ProviderKey")]
		public string ProviderKey { get; set; }
	}

	public class ExternalTokenLogin
	{
		[JsonProperty("LoginProvider")]
		public string LoginProvider { get; set; }

		[JsonProperty("ExternalAccessToken")]
		public string ExternalAccessToken { get; set; }

		[JsonProperty("ExternalAccessSecret")]
		public string ExternalAccessSecret { get; set; }
	}

	public class TwitterLoginRequest
	{
		[JsonProperty("SecretToken")]
		public string SecretToken { get; set; }

		[JsonProperty("Token")]
		public string Token { get; set; }
	}
}
