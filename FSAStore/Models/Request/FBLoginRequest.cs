﻿using System;

namespace FSAStore.Models.Request
{
	public class FBLoginRequest
	{
		public string Token { get; set; }
	}
}
