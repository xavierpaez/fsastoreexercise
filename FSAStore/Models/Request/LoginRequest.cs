﻿using System;
using Newtonsoft.Json;

namespace FSAStore.Models
{
	public class LoginRequest
	{
		public string Username { get; set; }
		public string Password { get; set; }
	}
}
