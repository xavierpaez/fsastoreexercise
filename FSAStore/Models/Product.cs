﻿using System;
using Realms;

namespace FSAStore.Models
{
	public class Product : RealmObject
	{
		public int ItemId { get; set; }

		public string Name { get; set; }

		public int Quantity { get; set; }

		public double SalePrice { get; set; }

		public string Total
		{
			get
			{
				return $"$ {SalePrice * Quantity}";
			}
		}
		public string ShortDescription { get; set; }

		public string LongDescription { get; set; }

		public string ThumbnailImage { get; set; }

		public string MediumImage { get; set; }
	}
}
