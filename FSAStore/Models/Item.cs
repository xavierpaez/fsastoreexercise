﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Realms;

namespace FSAStore.Models
{
	public class Item
	{
		[JsonProperty("itemId")]
		public int ItemId { get; set; }

		[JsonProperty("parentItemId")]
		public int ParentItemId { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("salePrice")]
		public double SalePrice { get; set; }

		[JsonIgnore]
		public string Price
		{
			get
			{
				return $"$ {SalePrice}";
			}
		}

		[JsonProperty("upc")]
		public string Upc { get; set; }

		[JsonProperty("categoryPath")]
		public string CategoryPath { get; set; }

		[JsonProperty("longDescription")]
		public string LongDescription { get; set; }

		[JsonProperty("thumbnailImage")]
		public string ThumbnailImage { get; set; }

		[JsonProperty("mediumImage")]
		public string MediumImage { get; set; }

		[JsonProperty("largeImage")]
		public string LargeImage { get; set; }

		[JsonProperty("standardShipRate")]
		public double StandardShipRate { get; set; }

		[JsonProperty("marketplace")]
		public bool Marketplace { get; set; }

		[JsonProperty("modelNumber")]
		public string ModelNumber { get; set; }

		[JsonProperty("productUrl")]
		public string ProductUrl { get; set; }

		[JsonProperty("customerRating")]
		public string CustomerRating { get; set; }

		[JsonProperty("numReviews")]
		public int NumReviews { get; set; }

		[JsonProperty("customerRatingImage")]
		public string CustomerRatingImage { get; set; }

		[JsonProperty("categoryNode")]
		public string CategoryNode { get; set; }

		[JsonProperty("bundle")]
		public bool Bundle { get; set; }

		[JsonProperty("stock")]
		public string Stock { get; set; }

		[JsonProperty("addToCartUrl")]
		public string AddToCartUrl { get; set; }

		[JsonProperty("giftOptions")]
		public GiftOptions GiftOptions { get; set; }

		[JsonProperty("imageEntities")]
		public IList<ImageEntity> ImageEntities { get; set; }

		[JsonProperty("offerType")]
		public string OfferType { get; set; }

		[JsonProperty("isTwoDayShippingEligible")]
		public bool IsTwoDayShippingEligible { get; set; }

		[JsonProperty("availableOnline")]
		public bool AvailableOnline { get; set; }

		[JsonProperty("shortDescription")]
		public string ShortDescription { get; set; }

		[JsonProperty("msrp")]
		public double? Msrp { get; set; }

		[JsonProperty("productTrackingUrl")]
		public string ProductTrackingUrl { get; set; }

		[JsonProperty("sellerInfo")]
		public string SellerInfo { get; set; }

		[JsonProperty("affiliateAddToCartUrl")]
		public string AffiliateAddToCartUrl { get; set; }

		[JsonProperty("freeShippingOver50Dollars")]
		public bool? FreeShippingOver50Dollars { get; set; }
	}
}
