﻿using System;
using Realms;

namespace FSAStore.Models
{
	public class Cart : RealmObject
	{
		public int ItemsInCart { get; set; }
	}
}
