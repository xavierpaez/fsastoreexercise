﻿using FSAStore.Services;
using FSAStore;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using Microsoft.Practices.Unity;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using Prism.Unity;
using FSAStore.Helpers;
using Xamarin.Forms.Xaml;
using Xamarin.Forms;
using FSAStore.Views;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace FSAStore
{
	public partial class App : PrismApplication
	{
		static IApiService<IFSAStoreApi> fsaStoreApi = new ApiService<IFSAStoreApi>(Config.ApiUrl);
		static IApiService<IBack4App> back4app = new ApiService<IBack4App>(Config.ApiBack4AppUrl);

		public App(IPlatformInitializer initializer = null) : base(initializer) { }

		protected override void OnInitialized()
		{
			InitializeComponent();
			NavigationService.NavigateAsync(NavigationConstants.Login);
		}

		protected override void RegisterTypes()
		{
			Container.RegisterInstance<IConnectivity>(CrossConnectivity.Current);
			Container.RegisterInstance<IApiService<IFSAStoreApi>>(fsaStoreApi, new ExternallyControlledLifetimeManager());
			Container.RegisterInstance<IApiService<IBack4App>>(back4app, new ExternallyControlledLifetimeManager());
			Container.RegisterType<IApiManager, ApiManager>();
			Container.RegisterType<IDBManager, DBManager>();
			Container.RegisterTypeForNavigation<NavigationPage>();
			Container.RegisterTypeForNavigation<LoginPage>();
			Container.RegisterTypeForNavigation<RootTabbedPage>();
			Container.RegisterTypeForNavigation<ProductsPage>();
			Container.RegisterTypeForNavigation<ShoppingCartPage>();
			Container.RegisterTypeForNavigation<SettingsPage>();
		}
	}
}
