// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace FSAStore.Helpers
{
	/// <summary>
	/// This is the Settings static class that can be used in your Core solution or in any
	/// of your client applications. All settings are laid out the same exact way with getters
	/// and setters. 
	/// </summary>
	public static class Settings
	{
		private static ISettings AppSettings
		{
			get
			{
				return CrossSettings.Current;
			}
		}

		#region Setting Constants

		private const string SettingsKey = "settings_key";
		private const string TokenKey = "token_key";
		private const string NameKey = "name_key";
		private const string LastNameKey = "lastname_key";
		private const string EmailKey = "email_key";
		private const string ProductsKey = "products_key";
		private const string TotalKey = "total_key";

		private static readonly string SettingsDefault = string.Empty;
		#endregion


		public static string GeneralSettings
		{
			get
			{
				return AppSettings.GetValueOrDefault<string>(SettingsKey, SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue<string>(SettingsKey, value);
			}
		}

		public static string Token
		{
			get
			{
				return AppSettings.GetValueOrDefault<string>(TokenKey, SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue<string>(TokenKey, value);
			}
		}

		public static string Names
		{
			get
			{
				return AppSettings.GetValueOrDefault<string>(NameKey, SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue<string>(NameKey, value);
			}
		}

		public static string LastName
		{
			get
			{
				return AppSettings.GetValueOrDefault<string>(LastNameKey, SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue<string>(LastNameKey, value);
			}
		}

		public static string Email
		{
			get
			{
				return AppSettings.GetValueOrDefault<string>(EmailKey, SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue<string>(EmailKey, value);
			}
		}

		public static int Products
		{
			get
			{
				return AppSettings.GetValueOrDefault<int>(ProductsKey, 0);
			}
			set
			{
				AppSettings.AddOrUpdateValue<int>(ProductsKey, value);
			}
		}

		public static double Total
		{
			get
			{
				return AppSettings.GetValueOrDefault<double>(TotalKey, 0);
			}
			set
			{
				AppSettings.AddOrUpdateValue<double>(TotalKey, value);
			}
		}

	}
}