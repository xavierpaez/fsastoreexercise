﻿using System;

namespace FSAStore.Helpers
{
	public class NavigationConstants
	{
		public const string Splash = "SplashPage";
		public const string Login = "LoginPage";
		public const string Home = "RootTabbedPage";
		public const string ShoppingCart= "ShoppingCartPage";
		public const string ProductsPage = "ProductsPage";
	}
}
