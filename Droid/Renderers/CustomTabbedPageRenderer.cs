﻿using System;
using Android.Widget;
using Android.App;
using Android.Graphics.Drawables;
using Android.Views;
using System.ComponentModel;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.Graphics;
using Android.Support.V7.App;
using Android.OS;
using Xamarin.Forms.Platform.Android.AppCompat;
using Android.Support.V4.View;
using Android.Content;
using Android.Util;
using Android.Support.Design.Widget;
using FSAStore.Droid.Renderers;

[assembly: ExportRenderer(typeof(FSAStore.Views.RootTabbedPage), typeof(CustomTabbedPageRenderer))]
namespace FSAStore.Droid.Renderers
{
	public class CustomTabbedPageRenderer : TabbedPageRenderer
	{
		ViewPager viewPager;
		Android.Support.Design.Widget.TabLayout tabs;
		TabbedPage tabbedPage;
		Activity activity;

		protected override void OnLayout(bool changed, int l, int t, int r, int b)
		{
			InvertLayoutThroughScale();

			base.OnLayout(changed, l, t, r, b);
		}

		private void InvertLayoutThroughScale()
		{
			ViewGroup.ScaleY = -1.0f;

			TabLayout tabLayout = null;
			ViewPager viewPager = null;

			for (int i = 0; i < ChildCount; ++i)
			{
				Android.Views.View view = (Android.Views.View)GetChildAt(i);
				//view.SetBackgroundColor(Android.Graphics.Color.Yellow);
				view.LayoutParameters = new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.MatchParent,
				ViewGroup.LayoutParams.MatchParent);

				if (view is TabLayout) tabLayout = (TabLayout)view;
				else if (view is ViewPager) viewPager = (ViewPager)view;
			}
			if (ViewGroup.LayoutParameters is ViewGroup.MarginLayoutParams)
			{
				ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams)ViewGroup.LayoutParameters;
				p.SetMargins(0, 0, 0, 0);
				ViewGroup.RequestLayout();
			}
			tabLayout.SetY(0.0f);
			viewPager.SetY(0.0f);
			viewPager.PageMargin = 0;
			viewPager.SetClipToPadding(false);
			viewPager.SetPageMarginDrawable(null);
			tabLayout.ScaleY = viewPager.ScaleY = -1.0f;

			ViewGroup.SetBackgroundColor(Android.Graphics.Color.Transparent);
			viewPager.SetPadding(0, 0, 0, 0);

			viewPager.RequestLayout();

		}

		protected override void OnElementChanged(ElementChangedEventArgs<TabbedPage> e)
		{
			base.OnElementChanged(e);
		}
	}
}

