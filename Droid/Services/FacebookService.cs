﻿using System;
using System;
using FSAStore.Services;
using Xamarin.Facebook.Login;
using Newtonsoft.Json.Linq;
using Xamarin.Facebook;
using Java.Lang;
using JObject = Java.Lang.Object;
using System.Threading.Tasks;
using System.Linq;
using Android.App;
using Xamarin.Facebook.Share.Model;
using Android.Graphics;
using Xamarin.Facebook.Share;
using Facebook;
using Android.OS;
using System.IO;
using System.Collections.Generic;
using Android.Runtime;
using Plugin.CurrentActivity;

namespace FSAStore.Droid
{
	public class FacebookService : JObject, IFacebookService
	{
		ICallbackManager mCallbackManager;
		//Activity mActivity;
		FacebookCallback<SharerResult> shareCallback;
		FacebookCallback<LoginResult> loginCallback;

		FacebookPendingAction<Dictionary<string, object>> pendingAction;

		public event EventHandler<FBEventArgs<Dictionary<string, object>>> OnUserData = delegate { };
		public event EventHandler<FBEventArgs<bool>> OnLogin = delegate { };
		public event EventHandler<FBEventArgs<bool>> OnLogout = delegate { };
		public event EventHandler<FBEventArgs<Dictionary<string, object>>> OnSharing = delegate { };

		public string[] ActivePermissions
		{
			get
			{
				return AccessToken.CurrentAccessToken == null ? new string[] { } : AccessToken.CurrentAccessToken.Permissions.Select(p => $"{p}").ToArray();
			}
		}

		public string[] DeclinedPermissions
		{
			get
			{

				return AccessToken.CurrentAccessToken == null ? new string[] { } : AccessToken.CurrentAccessToken.DeclinedPermissions.Select(p => $"{p}").ToArray();
			}
		}

		public bool IsLoggedIn
		{
			get
			{
				return AccessToken.CurrentAccessToken != null;
			}
		}

		public string ActiveToken
		{
			get
			{
				return AccessToken.CurrentAccessToken?.Token ?? string.Empty;
			}
		}


		public string ActiveUserId
		{
			get
			{
				return AccessToken.CurrentAccessToken?.UserId ?? string.Empty;
			}
		}




		public FacebookService(ICallbackManager callbackManager)
		{
			mCallbackManager = callbackManager;
			//mActivity = activity;

			loginCallback = new FacebookCallback<LoginResult>
			{
				HandleSuccess = loginResult =>
				{

					if (loginResult.AccessToken != null)
					{

						OnLogin(this, new FBEventArgs<bool>(true, FacebookActionStatus.Completed));
						pendingAction?.Execute();

						pendingAction = null;
					}
				},
				HandleCancel = () =>
				{
					OnLogin(this, new FBEventArgs<bool>(false, FacebookActionStatus.Canceled));
					//Handle any cancel the user has perform
					Console.WriteLine("User cancel de login operation");

					pendingAction?.Execute();

					pendingAction = null;
				},
				HandleError = loginError =>
				{
					OnLogin(this, new FBEventArgs<bool>(false, FacebookActionStatus.Error, loginError.Cause.Message));
					//Handle any error happends here
					Console.WriteLine("Operation throws an error: " + loginError.Cause.Message);

					pendingAction?.Execute();
					pendingAction = null;
				}
			};


			shareCallback = new FacebookCallback<SharerResult>
			{
				HandleSuccess = shareResult =>
				{
					Console.WriteLine("HelloFacebook: Success!");

					if (shareResult.PostId != null)
					{
						Dictionary<string, object> parameters = new Dictionary<string, object>();

						parameters.Add("postId", shareResult.PostId);

						OnSharing(this, new FBEventArgs<Dictionary<string, object>>(parameters, FacebookActionStatus.Completed));
					}
				},
				HandleCancel = () =>
				{
					Console.WriteLine("HelloFacebook: Canceled");
					OnSharing(this, new FBEventArgs<Dictionary<string, object>>(null, FacebookActionStatus.Canceled));

				},
				HandleError = shareError =>
				{
					Console.WriteLine("HelloFacebook: Error: {0}", shareError);

					OnSharing(this, new FBEventArgs<Dictionary<string, object>>(null, FacebookActionStatus.Error, shareError.Message));
				}
			};

			LoginManager.Instance.RegisterCallback(mCallbackManager, loginCallback);
		}

		public void Logout()
		{
			LoginManager.Instance.LogOut();
			AccessToken.CurrentAccessToken = null;
			Profile.CurrentProfile = null;
			OnLogout(this, new FBEventArgs<bool>(true, FacebookActionStatus.Completed));
		}


		public bool VerifyPermission(string permission)
		{

			return (AccessToken.CurrentAccessToken != null) && (AccessToken.CurrentAccessToken.Permissions.Contains(permission));

		}



		async void PerformAction(Action<Dictionary<string, object>> action, Dictionary<string, object> parameters, FacebookPermissionType permissionType, string[] permissions)
		{
			pendingAction = null;

			bool authorized = HasPermissions(permissions);

			if (!authorized)
			{
				pendingAction = new FacebookPendingAction<Dictionary<string, object>>(action, parameters);
				await LoginAsync(permissions, permissionType);
			}
			else {
				action(parameters);
			}


		}


		public bool HasPermissions(string[] permissions)
		{
			if (!IsLoggedIn)
				return false;

			var currentPermissions = AccessToken.CurrentAccessToken.Permissions;

			return permissions.All(p => !AccessToken.CurrentAccessToken.DeclinedPermissions.Contains(p));
		}

		public void RequestUserData(string[] fields, string[] permissions, FacebookPermissionType permissionType = FacebookPermissionType.Read)
		{
			Dictionary<string, object> parameters = new Dictionary<string, object>()
			{
				{"fields",fields}
			};

			PerformAction(RequestUserData, parameters, FacebookPermissionType.Read, permissions);
		}

		async void RequestUserData(Dictionary<string, object> fieldsDictionary)
		{
			string[] fields = new string[] { };
			var extraParams = string.Empty;

			if (fieldsDictionary != null && fieldsDictionary.ContainsKey("fields"))
			{
				fields = fieldsDictionary["fields"] as string[];
				if (fields.Length > 0)
					extraParams = $"?fields={string.Join(",", fields)}";
			}

			if (AccessToken.CurrentAccessToken != null)
			{
				FacebookClient fb = new FacebookClient(AccessToken.CurrentAccessToken.Token);

				var userInfo = (await fb.GetTaskAsync($"/me{extraParams}")) as IDictionary<string, object>;
				Dictionary<string, object> userData = new Dictionary<string, object>();

				for (int i = 0; i < fields.Length; i++)
				{
					if (userInfo.ContainsKey(fields[i]))
						userData.Add(fields[i], $"{userInfo[fields[i]]}");
				}
				userData.Add("user_id", AccessToken.CurrentAccessToken.UserId);
				userData.Add("token", AccessToken.CurrentAccessToken.Token);
				OnUserData(this, new FBEventArgs<Dictionary<string, object>>(userData, FacebookActionStatus.Completed));
			}
			else {
				OnUserData(this, new FBEventArgs<Dictionary<string, object>>(null, FacebookActionStatus.Canceled));
			}

		}

		public Task<bool> LoginAsync(string[] permissions, FacebookPermissionType permissionType = FacebookPermissionType.Read)
		{
			var retVal = IsLoggedIn;
			if (!retVal || !HasPermissions(permissions))
			{

				if (permissionType == FacebookPermissionType.Read)
				{
					LoginManager.Instance.LogInWithReadPermissions(CrossCurrentActivity.Current.Activity, permissions.ToList());
				}
				else
				{
					LoginManager.Instance.LogInWithPublishPermissions(CrossCurrentActivity.Current.Activity, permissions.ToList());
				}

			}
			else
			{

				OnLogin(this, new FBEventArgs<bool>(retVal, FacebookActionStatus.Completed));
				pendingAction?.Execute();

				pendingAction = null;

			}
			return Task.FromResult(true);
		}




	}

	/// <summary>
	/// FacebookCallback<TResult> class which will handle any result the FacebookActivity returns.
	/// </summary>
	/// <typeparam name="TResult">The callback result's type you will handle</typeparam>
	public class FacebookCallback<TResult> : Java.Lang.Object, IFacebookCallback where TResult : Java.Lang.Object
	{
		public Action HandleCancel { get; set; }
		public Action<FacebookException> HandleError { get; set; }
		public Action<TResult> HandleSuccess { get; set; }

		public void OnCancel()
		{
			var c = HandleCancel;
			if (c != null)
				c();
		}

		public void OnError(FacebookException error)
		{
			var c = HandleError;
			if (c != null)
				c(error);
		}

		public void OnSuccess(Java.Lang.Object result)
		{
			var c = HandleSuccess;
			if (c != null)
				c(result.JavaCast<TResult>());
		}
	}
}
