﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin.Facebook;
using FSAStore.Services;
using Prism.Unity;
using Acr.UserDialogs;
using Plugin.Permissions;
using Microsoft.Practices.Unity;
using FFImageLoading.Forms.Droid;

namespace FSAStore.Droid
{
	[Activity(Label = "FSAStore.Droid", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		public static ICallbackManager CallbackManager;
		static FacebookService fbService;

		protected override void OnCreate(Bundle bundle)
		{
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate(bundle);

			FacebookSdk.SdkInitialize(this.ApplicationContext);
			CallbackManager = CallbackManagerFactory.Create();
			CachedImageRenderer.Init();
			global::Xamarin.Forms.Forms.Init(this, bundle);

			UserDialogs.Init(this);

			LoadApplication(new App(new AndroidInitializer()));
		}

		public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
		{
			PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
		}

		public class AndroidInitializer : IPlatformInitializer
		{
			public void RegisterTypes(IUnityContainer container)
			{
				fbService = new FacebookService(CallbackManager);
				container.RegisterInstance<IUserDialogs>(UserDialogs.Instance, new ExternallyControlledLifetimeManager());
				container.RegisterInstance<IFacebookService>(fbService, new ExternallyControlledLifetimeManager());
			}
		}

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent intent)
		{
			base.OnActivityResult(requestCode, resultCode, intent);
			CallbackManager.OnActivityResult(requestCode, (int)resultCode, intent);

			if (resultCode == Result.FirstUser)
			{
				var token = intent.GetStringExtra("facebookToken");
			}

			try
			{
				if (resultCode == Result.Ok)
				{
					if (intent != null)
					{

						global::Android.Net.Uri uri = intent.Data;
					}
				}
				//Send the paths to forms
			}
			catch (Exception ex)
			{

				Console.WriteLine(ex.ToString());
				Toast.MakeText(Xamarin.Forms.Forms.Context, "Unable to open, error:" + ex.ToString(), ToastLength.Long).Show();
			}


		}
	}
}
